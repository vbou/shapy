
w = 2**32

def ch(x, y, z):
    return ((x & y) ^ (~x & z)) % w

def maj(x, y, z):
    return ((x & y) ^ (x & z) ^ (y & z)) % w

def rotr(x,n):
    return (((x >> n) % w) | ((x << (32-n)) % w)) % w

def Sigma0(x):
    return (rotr(x,2) ^ rotr(x,13) ^ rotr(x,22)) % w

def Sigma1(x):
    return (rotr(x,6) ^ rotr(x,11) ^ rotr(x,25)) % w

def sigma0(x):
    return (rotr(x,7) ^ rotr(x,18) ^ ((x >> 3) % w)) % w

def sigma1(x):
    return (rotr(x,17) ^ rotr(x,19) ^ ((x >> 10) % w)) % w

K = [
    0x428a2f98,0x71374491,0xb5c0fbcf,0xe9b5dba5,0x3956c25b,0x59f111f1,0x923f82a4,0xab1c5ed5, 
    0xd807aa98,0x12835b01,0x243185be,0x550c7dc3,0x72be5d74,0x80deb1fe,0x9bdc06a7,0xc19bf174, 
    0xe49b69c1,0xefbe4786,0x0fc19dc6,0x240ca1cc,0x2de92c6f,0x4a7484aa,0x5cb0a9dc,0x76f988da,
    0x983e5152,0xa831c66d,0xb00327c8,0xbf597fc7,0xc6e00bf3,0xd5a79147,0x06ca6351,0x14292967,
    0x27b70a85,0x2e1b2138,0x4d2c6dfc,0x53380d13,0x650a7354,0x766a0abb,0x81c2c92e,0x92722c85,
    0xa2bfe8a1,0xa81a664b,0xc24b8b70,0xc76c51a3,0xd192e819,0xd6990624,0xf40e3585,0x106aa070,
    0x19a4c116,0x1e376c08,0x2748774c,0x34b0bcb5,0x391c0cb3,0x4ed8aa4a,0x5b9cca4f,0x682e6ff3,
    0x748f82ee,0x78a5636f,0x84c87814,0x8cc70208,0x90befffa,0xa4506ceb,0xbef9a3f7,0xc67178f2
]

H = [[0x6a09e667,0xbb67ae85,0x3c6ef372,0xa54ff53a,0x510e527f,0x9b05688c,0x1f83d9ab,0x5be0cd19]]

def padding(m):
    ltotal = len(m)
    l = len(m) % 512
    # Add one 1 bit
    result = m + '1'
    # Add n 0 bits
    for i in range(1,448-l):
        result += '0'
    # Add 64 bits 
    result += bin(ltotal)[2:].zfill(64)
    return result

def split(m, size):
    chunks = len(m)
    chunk_size = size
    return [ m[i:i+chunk_size] for i in range(0, chunks, chunk_size) ]

# abc in ASCII
m = '011000010110001001100011'
m = padding(m)
blocks = split(m, 512)

for i in range(1,len(blocks)+1):

    subBlocks = split(blocks[i-1],32)

    # Message schedule
    W = [None] * 64
    for t in range(0,64):
        if t <= 15:
            W[t] = int(subBlocks[t],2)
        else:
            W[t] = (sigma1(W[t-2]) + W[t-7] + sigma0(W[t-15]) + W[t-16]) % w

    # Init working variables
    a = H[0][0]
    b = H[0][1]
    c = H[0][2]
    d = H[0][3]
    e = H[0][4]
    f = H[0][5]
    g = H[0][6]
    h = H[0][7]

    for t in range(0,64):

        t1 = (h + Sigma1(e) + ch(e,f,g) + K[t] + W[t]) % w
        t2 = (Sigma0(a) + maj(a,b,c)) % w
        h = g
        g = f
        f = e
        e = (d + t1) % w
        d = c
        c = b
        b = a
        a = (t1 + t2) % w

        print(hex(a)[2:].zfill(8) + ' ' + hex(b)[2:].zfill(8) + ' ' + hex(c)[2:].zfill(8) + ' ' + hex(d)[2:].zfill(8) + ' ' + hex(e)[2:].zfill(8) + ' ' + hex(f)[2:].zfill(8) + ' ' + hex(g)[2:].zfill(8) + ' ' + hex(h)[2:].zfill(8))


    H.append([])
    H[i].append((a + H[i-1][0]) % w)
    H[i].append((b + H[i-1][1]) % w)
    H[i].append((c + H[i-1][2]) % w)
    H[i].append((d + H[i-1][3]) % w)
    H[i].append((e + H[i-1][4]) % w)
    H[i].append((f + H[i-1][5]) % w)
    H[i].append((g + H[i-1][6]) % w)
    H[i].append((h + H[i-1][7]) % w)

final = ''
for y in range(0,8):
    final = final + hex(H[len(H)-1][y])[2:].zfill(8)
print(final)